package com.example.shopping;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ShoppingCartAppTest {

    ShoppingCartApp shoppingCartApp = ShoppingCartApp.getInstance();

    @BeforeEach
    void init() {
        shoppingCartApp.emptyCart();
    }

    @AfterEach
    void tearDown() {
        shoppingCartApp.emptyCart();
    }

    /**
     * AC 0 : Add a single product to a cart.
     */
    @Test
    public void addProductToCart() {
        //confirm empty cart
        assertEquals(0, shoppingCartApp.getTotalCartItems().size());

        //add a cart item.
        String productCode = "dove1_AC_0";
        String productName = "Dove Soap 1";
        final BigDecimal price1 = new BigDecimal(39.99);
        Product product1 = new Product(productCode, productName, price1);
        CartItem cartItem = new CartItem(product1, 1);
        shoppingCartApp.addItemToShoppingCart(cartItem);

        // The shopping cart should a single line item with 1 Dove Soap with a unit price of _39.99_
        List<CartItem> shoppingCartItems = shoppingCartApp.getTotalCartItems();
        assertEquals(1, shoppingCartItems.size());
        Assertions.assertNotNull(shoppingCartApp.getItemFromShoppingCart(productCode));
        CartItem cartItem1 = shoppingCartApp.getItemFromShoppingCart(productCode);
        assertEquals(product1.getCode(), cartItem1.getProduct().getCode());
        final BigDecimal expectedCartPrice = new BigDecimal(39.99).setScale(2, RoundingMode.HALF_UP);
        BigDecimal actualCartPrice = shoppingCartApp.getTotalAmount();
        assertEquals(expectedCartPrice, actualCartPrice);
    }

    /**
     * AC 1 : Add many products (same type of product) to Cart
     */
    @Test
    public void addManyProductsToCart() {
        //confirm empty cart
        assertEquals(0, shoppingCartApp.getTotalCartItems().size());

        String productCode = "dove1_AC_0";
        String productName = "Dove Soap 1";
        BigDecimal price1 = new BigDecimal(39.99);
        Product product = new Product(productCode, productName, price1);

        CartItem cart1Item = new CartItem(product, 5);
        shoppingCartApp.addItemToShoppingCart(cart1Item);

        CartItem cart2Item = new CartItem(product, 3);
        shoppingCartApp.addItemToShoppingCart(cart2Item);

        int itemQty = shoppingCartApp.getItemFromShoppingCart(productCode).getQty();
        assertEquals(8, itemQty);
        BigDecimal expectedTotalAmount = new BigDecimal(319.92)
                .setScale(2, RoundingMode.HALF_UP);
        assertEquals(expectedTotalAmount, shoppingCartApp.getTotalAmount());
    }

    /**
     * AC 2 : Calculate tax rate with many products
     */
    @Test
    public void calculateTaxRateWithManyProducts() {
        //confirm empty cart
        assertEquals(0, shoppingCartApp.getTotalCartItems().size());

        //_Dove Soap_ with a unit price of _39.99_
        String product1Code = "Dove_Soap_1";
        String product1Name = "Dove Soap";
        final BigDecimal product1price = new BigDecimal(39.99);
        Product product1 = new Product(product1Code, product1Name, product1price);
        CartItem cart1Item = new CartItem(product1, 2);
        shoppingCartApp.addItemToShoppingCart(cart1Item);

        //_Axe Deo_ with a unit price of _99.99_
        String product2Code = "Axe_Deo_1";
        String product2Name = "Axe Deo";
        final BigDecimal product2price = new BigDecimal(99.99);
        Product product2 = new Product(product2Code, product2Name, product2price);
        CartItem cart2Item = new CartItem(product2, 2);
        shoppingCartApp.addItemToShoppingCart(cart2Item);

        BigDecimal actualTotalAmount = shoppingCartApp.getTotalAmount();
        BigDecimal actualTax = shoppingCartApp.getTax();
        BigDecimal actualTotalPayableAmount = shoppingCartApp.getPayableAmount();

        assertEquals(new BigDecimal(279.96).setScale(2,RoundingMode.HALF_UP), actualTotalAmount);
        assertEquals(new BigDecimal(35.00).setScale(2,RoundingMode.HALF_UP), actualTax);
        assertEquals(new BigDecimal(314.96).setScale(2,RoundingMode.HALF_UP), actualTotalPayableAmount);
    }

}