package com.example.shopping;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class ShoppingCartApp {

    private static ShoppingCartApp INSTANCE;
    private Map<String, CartItem> itemsMap;

    public ShoppingCartApp() {
        this.itemsMap = new HashMap<>();
    }

    public synchronized static ShoppingCartApp getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ShoppingCartApp();
        }
        return INSTANCE;
    }

    private void clearValues() {
        this.itemsMap.clear();
    }

    public boolean addItemToShoppingCart(CartItem cartItem) {
        CartItem c = this.itemsMap.get(cartItem.getProduct().getCode());
        if (c != null) {
            int cQty = c.getQty();
            int cartItemQty = cartItem.getQty();
            cartItem.setQty(cQty + cartItemQty);
            BigDecimal productPrice = cartItem.getProduct().getPrice();
            BigDecimal productQuantity = new BigDecimal(cartItem.getQty());
            cartItem.setAmount(productPrice.multiply(productQuantity).
                    setScale(2, RoundingMode.HALF_UP));
        }
        this.itemsMap.put(cartItem.getProduct().getCode(), cartItem);
        return true;
    }

    public CartItem getItemFromShoppingCart(String productCode) {
        return this.itemsMap.get(productCode);
    }

    public void emptyCart() {
        clearValues();
    }

    public BigDecimal getTotalAmount() {
        BigDecimal totalAmount = BigDecimal.ZERO;
        final Iterator<String> iterator = itemsMap.keySet().iterator();

        while (iterator.hasNext()) {
            final BigDecimal currentOrderPrice =
                    itemsMap.get(iterator.next()).getAmount();
            totalAmount = totalAmount.add(currentOrderPrice);
        }

        return totalAmount.setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal getPayableAmount() {
        BigDecimal payableAmount = BigDecimal.ZERO;
        payableAmount = payableAmount.add(getTotalAmount());
        payableAmount = payableAmount.add(getTax());
        return payableAmount.setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal getTax() {
        BigDecimal totalTax = BigDecimal.ZERO;
        totalTax = totalTax.add(new BigDecimal(0.125));
        totalTax = totalTax.multiply(getTotalAmount());
        return totalTax.setScale(2, RoundingMode.HALF_UP);
    }

    public List<CartItem> getTotalCartItems() {
        return new ArrayList<>(itemsMap.values());
    }

}
