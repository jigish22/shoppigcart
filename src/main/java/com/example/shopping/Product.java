package com.example.shopping;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Product {

    private final BigDecimal price;
    private String code;
    private String name;

    public Product(String code, String name, BigDecimal price) {

        if (code == null) {
            throw new IllegalArgumentException("Code can not be a null value!");
        }
        if (code.isEmpty()) {
            throw new IllegalArgumentException("Please enter valid value for a code! ");
        }

        if (name == null) {
            throw new IllegalArgumentException("name can not be a null value!");
        }

        if (name.isEmpty()) {
            throw new IllegalArgumentException("Please enter valid value for a name! ");
        }

        if (price == null) {
            throw new IllegalArgumentException("price can not be a null value!");
        }

        if (price.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("Please enter valid value for a price!");
        }

        this.code = code;
        this.name = name;
        this.price = price.setScale(2, RoundingMode.HALF_UP);
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
