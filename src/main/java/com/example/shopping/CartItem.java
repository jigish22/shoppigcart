package com.example.shopping;

import java.math.BigDecimal;

public class CartItem {
    Product product;
    int qty;
    private BigDecimal amount;

    public CartItem(Product product, int qty) {

        if (product == null) {
            throw new IllegalArgumentException("Product can not be null!");
        }
        if (qty <= 0) {
            throw new IllegalArgumentException("Quantity should be equal or greater than 1!");
        }

        this.product = product;
        this.qty = qty;
        this.amount = product.getPrice().multiply(BigDecimal.valueOf(qty));
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
